
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Button, Image, Grid } from 'semantic-ui-react'
import { FormattedMessage as FM } from 'react-intl'

import FormPage from './FormPage'
import TextField from 'components/Form/TextField'
import client from 'client'
import store from 'store'
import presikLogo from 'assets/img/presik-login.png'
import { color } from 'theme'

class LoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      database: '',
      username: '',
      password: '',
      user: null,
      formMessage: null
    }
    this.onClose = this.onClose.bind(this)
    this.handleStartSession = this.handleStartSession.bind(this)
  }

  handleStartSession = async () => {
    let { username, database, password }  = this.state
    const history =  this.props.history
    let res = await client.proxy.login(database, username, password)

    if (res && res.user) {
      res['db'] = database
      store.set('ctxSession', res)
      history.push(`/${database}/admin/dashboard`)
      this.props.handleStartSession()
    } else {
      this.setState({
        formMessage: 'login.invalid_user_password'
      })
    }
  }

  onClose () {
    this.setState({
      openModal: false
    })
  }

  handleChange = (field, value, action) => {
    let toState = {}
    toState[field] = value
    this.setState(toState)
  }

  render () {
    const { formMessage } = this.state

    return (
      <Grid centered columns={1}>
        <Grid.Column mobile={16} tablet={8} computer={4} >
          <Image src={presikLogo} alt='PRESIK LOGO' width='280' style={styles.img}/>
          <FormPage
            title='login.start_session'
            formMessage={formMessage}
            handleClose={this.onClose}
          >
            <TextField
              type='text'
              name='database'
              label={<FM id='login.database' />}
              onChange={this.handleChange}
              style={styles.text}
              variant='outlined'
            />
            <TextField
              type='text'
              name='username'
              label={<FM id='login.username' />}
              onChange={this.handleChange}
              variant='outlined'
              style={styles.text}
            />
            <TextField
              name='password'
              label={<FM id='login.password' />}
              variant='outlined'
              type='password'
              style={styles.text}
              onChange={this.handleChange}
            />
            <Grid.Column width={16} style={styles.col_button}>
              <Button
                // color='olive'
                style={styles.button}
                onClick={this.handleStartSession}>
                <FM id='login.button_enter' />
              </Button>
            </Grid.Column>
            <a style={styles.forgot} href='/'><FM id='login.forgot_password' /></a>
          </FormPage>
          <Grid.Column width={16} style={styles.col_text}>
            <p style={styles.footer}>Copyright© Presik SAS 2020</p>
          </Grid.Column>
        </Grid.Column>
      </Grid>
    )
  }
}

const styles = {
  button: {
    height: 50,
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
    color: color.grayLight,
    backgroundColor: '#0f1b28',
  },
  col_text: {
    paddingTop: 10,
    margin: 10,
    display: 'flex',
    justifyContent: 'center'
  },
  col_button: {
    paddingRight: 10,
  },
  footer: {
    color: color.grayDark
  },
  forgot: {
    marginTop: 20,
  },
  text: {
    width: '100%',
    backgroundColor: 'white'
  },
  img: {
    marginBottom: 15,
    marginTop: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
    display: 'block'
  }
}

export default withRouter(LoginScreen)
